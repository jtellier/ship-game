import P5 from "p5";
import {
    Coordinates,
    getCoordinatesCenter,
    coordsRote,
    lscale,
    addVector,
    c,
    Vector,
    getDistance,
    moveCoordinatesAtPosition,
    lscalec,
    cc,
} from "./geometry";
import { Ship } from "./ship";

type State = {
    ship: Coordinates[],
    collisionRadius: number,
    shipScreen: Coordinates[],
    starCenter: Coordinates,
    direction: Coordinates,
    stats: Stats,
    consumedScraps: string[],
    minions: Ship[],
    enemies: Ship[],
    destroyedEnemies: string[],
    isPaused: boolean,
    screenFactor: number,
}

let state: State;

let minionCoordinatesTemplate: Coordinates[] = [
    c(30, 75),
    c(60, 20),
    c(90, 75),
];
let enemyCoordinatesTemplate: Coordinates[] = lscalec([
    ...lscalec(
        [
            c(30, 75),
            c(60, 20),
            c(90, 75),
        ],
        1,
    ),
    ...lscalec(
        [
            c(30, 40),
            c(60, 95),
            c(90, 40)
        ],
        0.7,
    ),
], 0.5);

type Stats = {
    scraps: number,
};

export function rotateToTarget(coords: Coordinates[], target: Coordinates, p5: P5) {
    const center = getCoordinatesCenter(coords);

    const shipRadians = p5.atan2(coords[1].x - center.x, coords[1].y - center.y);
    const shipDegree = (shipRadians * (180 / Math.PI) * -1) + 90;
    coordsRote(coords, center, -shipDegree, p5);

    const mC = c(target.x, target.y);
    coordsRote([mC], center, -shipDegree, p5);

    const mradians = p5.atan2(mC.x - center.x, mC.y - center.y);
    const mdegree = (mradians * (180 / Math.PI) * -1) + 90;
    coordsRote(coords, center, mdegree, p5);
    coordsRote(coords, center, shipDegree, p5);
}

function initializeObjects(p5: P5) {
    p5.noLoop();
    let ship = [
        c(30, 75),
        c(58, 20),
        c(86, 75),
    ];
    const minSide = Math.min(p5.windowHeight, p5.windowWidth);
    lscalec(ship, minSide * 0.9 / 799);
    let collisionRadius = (
        ship.map(ship => ship.x).reduce((a, b) => Math.max(a, b), 0) - ship.map(ship => ship.x).reduce((a, b) => Math.min(a, b), 0),
        + ship.map(ship => ship.y).reduce((a, b) => Math.max(a, b), 0) - ship.map(ship => ship.y).reduce((a, b) => Math.min(a, b), 0)
    );
    const shipC = getCoordinatesCenter(ship);
    const screenC = c(p5.windowWidth / 2 - shipC.x, p5.windowHeight / 2 - shipC.y);
    addVector(ship, screenC);
    let shipScreen = JSON.parse(JSON.stringify(ship));
    let starCenter = JSON.parse(JSON.stringify(screenC));
    let direction = c(-1, -1);

    const title: HTMLElement = <HTMLElement>document.querySelector('#title');
    title.style.display = 'flex';
    const gameover: HTMLElement = <HTMLElement>document.querySelector('#game-over-title');
    gameover.style.display = 'none';
    const ui: HTMLElement = <HTMLElement>document.querySelector('#ui');
    ui.style.display = 'none';

    minionCoordinatesTemplate = moveCoordinatesAtPosition(minionCoordinatesTemplate, getCoordinatesCenter(minionCoordinatesTemplate));
    enemyCoordinatesTemplate = moveCoordinatesAtPosition(enemyCoordinatesTemplate, getCoordinatesCenter(enemyCoordinatesTemplate));

    let stats = {
        scraps: 15,
    };
    let consumedScraps: string[] = [];
    let minions: Ship[] = [];
    let enemies: Ship[] = [];
    let destroyedEnemies: string[] = [];
    let isPaused = false;
    let screenFactor = minSide / 799;
    state = {
        ship,
        collisionRadius,
        shipScreen,
        starCenter,
        direction,
        stats,
        consumedScraps,
        minions,
        enemies,
        destroyedEnemies,
        isPaused,
        screenFactor,
    }
    p5.loop();
    const minionsCount = <HTMLElement>document.querySelector('#minions-count');
    minionsCount.innerHTML = state.minions.length.toString();
}

function sinesPowerDistributed(coord: Coordinates, p5: P5, octaveA: number = 75 / 501, frequencyA: number = 27, octaveB: number = 55 / 501, frequencyB: number = 27) {
    const resolution = 18;
    const borderExtension = 15;
    const startY = Math.floor(coord.y / resolution) - borderExtension;
    const height = (p5.windowHeight + coord.y) / resolution + borderExtension;
    const startX = Math.floor(coord.x / resolution) - borderExtension;
    const width = (p5.windowWidth + coord.x) / resolution + borderExtension;
    const displacedPoints = [];
    for (let y = startY; y < height; y++) {
        for (let x = startX; x < width; x++) {
            const ppoint = c(x, y);
            ppoint.x = ppoint.x * resolution;
            ppoint.y = ppoint.y * resolution;
            ppoint.x += p5.sin(ppoint.x * octaveA) * frequencyA - p5.cos(ppoint.y * octaveB) * frequencyB;
            ppoint.y += p5.cos(ppoint.y * octaveA) * frequencyA - p5.sin(ppoint.x * octaveB) * frequencyB;
            displacedPoints.push({ grid: c(x, y), point: ppoint });
        }
    }
    return displacedPoints;
}

function showStars(coord: Coordinates, p5: P5) {
    p5.fill(255);
    p5.noStroke();
    sinesPowerDistributed(coord, p5)
        .forEach(({ grid, point }) => {
            if (p5.sin(point.x) + p5.cos(point.y) > 0.75) {
                const starProximity = 1 / (p5.sin(point.x) + p5.cos(point.y));
                p5.circle(
                    point.x - coord.x,
                    point.y - coord.y,
                    2 * starProximity,
                );
            }
        });
}

type Scrap = {
    coordinates: Coordinates[],
    id: string,
}

function setScraps(coord: Coordinates, p5: P5): Scrap[] {
    const distributionSpace = 0.003;
    const templateScrapCoords = [
        c(38, 31),
        c(86, 20),
        c(69, 63),
        c(30, 76),
    ];
    const minSide = Math.min(p5.windowHeight, p5.windowWidth);
    lscalec(templateScrapCoords, minSide * 0.3 / 799);
    const templateCenter = getCoordinatesCenter(templateScrapCoords);
    addVector(templateScrapCoords, c(-templateCenter.x, -templateCenter.y));
    addVector(templateScrapCoords, c(-templateCenter.x - coord.x, -templateCenter.y - coord.y));
    return <Scrap[]>sinesPowerDistributed(coord, p5)
        .map(({ grid, point }) => {
            const distribution = p5.sin(point.x) + p5.cos(point.y);
            if (distribution > 1.995 && distribution < 1.995 + distributionSpace) {
                const id: string = Math.round(point.x) + "#" + Math.round(point.y);
                if (state.consumedScraps.indexOf(id) === -1) {
                    const scrapCoords = [
                        c(point.x + p5.sin(grid.x) * 5 + templateScrapCoords[0].x, point.y + p5.cos(grid.x) * 5 + templateScrapCoords[0].y),
                        c(point.x + p5.sin(grid.y) * 5 + templateScrapCoords[1].x, point.y + p5.cos(grid.y) * 5 + templateScrapCoords[1].y),
                        c(point.x + p5.cos(grid.x) * 5 + templateScrapCoords[2].x, point.y + p5.sin(grid.x) * 5 + templateScrapCoords[2].y),
                        c(point.x + p5.cos(grid.y) * 5 + templateScrapCoords[3].x, point.y + p5.sin(grid.y) * 5 + templateScrapCoords[3].y),
                    ];
                    return { coordinates: scrapCoords, id: id };
                }
            }
        })
        .filter(point => point);
}

function setEnemies(currentEnemies: string[], destroyedEnemies: string[], coord: Coordinates, p5: P5): Ship[] {
    const distributionSpace = 0.0016;
    return <Ship[]>sinesPowerDistributed(coord, p5)
        .map(({ grid, point }) => {
            const distribution = p5.sin(point.x) + p5.cos(point.y);
            if (distribution > 1.896 && distribution < 1.896 + distributionSpace) {
                const id: string = Math.round(point.x) + "#" + Math.round(point.y) + "#";
                if (currentEnemies.indexOf(id) === -1 && destroyedEnemies.indexOf(id) === -1) {
                    return new Ship(
                        c(point.x - coord.x, point.y - coord.y),
                        0,
                        new Vector({x: 0, y: 0}, p5),
                        p5,
                        enemyCoordinatesTemplate,
                        (coords) => {
                            p5.stroke(200);
                            p5.fill(25);
                            
                            p5.triangle(
                                coords[3].x, coords[3].y,
                                coords[4].x, coords[4].y,
                                coords[5].x, coords[5].y,
                            );
                            p5.triangle(
                                coords[0].x, coords[0].y,
                                coords[1].x, coords[1].y,
                                coords[2].x, coords[2].y,
                            );
                            p5.circle(coords[1].x, coords[1].y, 2);
                        },
                        id,
                    );
                }
            }
        })
        .filter(ship => ship);
}

function displayScraps(scraps: Scrap[], p5: P5) {
    scraps.forEach((scrap: Scrap) => {
        p5.stroke(255);
        p5.strokeWeight(2);
        p5.fill(150);
        const coords = scrap.coordinates;
        p5.quad(
            coords[0].x, coords[0].y,
            coords[1].x, coords[1].y,
            coords[2].x, coords[2].y,
            coords[3].x, coords[3].y,
        );
    });
}

function collision(ship: Coordinates[], scraps: Scrap[]) {
    const shipCenter = getCoordinatesCenter(ship);
    scraps.forEach((scrap) => {
        const center = getCoordinatesCenter(scrap.coordinates);
        const newCenter = c(center.x - shipCenter.x, center.y - shipCenter.y);
        const distance = Math.sqrt(newCenter.x * newCenter.x + newCenter.y * newCenter.y);
        if (distance < state.collisionRadius) {
            state.stats.scraps += 1;
            state.consumedScraps.push(scrap.id);
            if (state.consumedScraps.length > state.collisionRadius) {
                state.consumedScraps = state.consumedScraps.slice(1);
            }
        }
    });
    state.enemies.forEach(enemy => {
        const center = enemy.coordinates;
        const newCenter = c(center.x - shipCenter.x, center.y - shipCenter.y);
        const distance = Math.sqrt(newCenter.x * newCenter.x + newCenter.y * newCenter.y);
        if (distance < state.collisionRadius) {
            state.stats.scraps -= 5;
            state.destroyedEnemies.push(enemy.id);
            if (state.destroyedEnemies.length > state.collisionRadius) {
                state.destroyedEnemies = state.destroyedEnemies.slice(1);
            }
        }
    });
    state.enemies = state.enemies.filter(enemy => state.destroyedEnemies.indexOf(enemy.id) === -1);
    const scrapsCount = <HTMLElement>document.querySelector('#scraps-count');
    scrapsCount.innerHTML = state.stats.scraps.toString();
}

const sketch = (p5: P5) => {
    p5.setup = () => {
        // put setup code here
        p5.createCanvas(p5.windowWidth, p5.windowHeight);
        initializeObjects(p5);
        const title: HTMLElement = <HTMLElement>document.querySelector('#title');
        const ui: HTMLElement = <HTMLElement>document.querySelector('#ui');
        title.addEventListener('click', () => {
            title.style.display = 'none';
            ui.style.display = 'block';
        })
        const reset: HTMLButtonElement = <HTMLButtonElement>document.querySelector('#reset');
        reset.addEventListener('click', () => {
            initializeObjects(p5);
        });
        const pause: HTMLButtonElement = <HTMLButtonElement>document.querySelector('#pause');
        pause.addEventListener('click', () => {
            if (!state.isPaused) {
                p5.noLoop();
            } else {
                p5.loop();
            }
            state.isPaused = !state.isPaused;
        });
        const buildShip: HTMLButtonElement = <HTMLButtonElement>document.querySelector('#build-ship');
        buildShip.addEventListener('click', (event) => {
            event.stopPropagation();
            event.preventDefault();
            if (state.stats.scraps > 3) {
                state.stats.scraps -= 3;
                const scrapsCount = <HTMLElement>document.querySelector('#scraps-count');
                scrapsCount.innerHTML = state.stats.scraps.toString();
                const minionShipCoordinates = JSON.parse(JSON.stringify(minionCoordinatesTemplate));
                lscale(minionShipCoordinates, 0.3);
                state.minions.push(new Ship(
                    cc(getCoordinatesCenter(state.shipScreen)),
                    0,
                    new Vector({ x: 0, y: 0 }, p5),
                    p5,
                    minionShipCoordinates,
                    (coords) => {
                        p5.stroke(50);
                        p5.fill(200);
                        p5.triangle(
                            coords[0].x, coords[0].y,
                            coords[1].x, coords[1].y,
                            coords[2].x, coords[2].y,
                        );
                        p5.circle(coords[1].x, coords[1].y, 2);
                    },
                    p5.millis().toString(),
                ));
                const minionsCount = <HTMLElement>document.querySelector('#minions-count');
                minionsCount.innerHTML = state.minions.length.toString();
            }
        });
        p5.frameRate(30);
        p5.loop();
    };

    p5.draw = () => {
        // put drawing code here
        p5.background(0);
        p5.stroke(200);
        p5.fill(255);
        addVector(state.ship, state.direction);
        addVector([state.starCenter], c(state.direction.x * 0.8, state.direction.y * 0.8));
        const speed = Math.sqrt(state.direction.x * state.direction.x + state.direction.y * state.direction.y);
        (<HTMLElement>document.querySelector('#speed')).innerHTML = speed.toFixed(0);

        const center = getCoordinatesCenter(state.ship);
        p5.noStroke();
        showStars(state.starCenter, p5);
        const scraps = setScraps(center, p5);
        displayScraps(scraps, p5);
        const foundEnemies = setEnemies(state.enemies.map(enemy => enemy.id), state.destroyedEnemies, center, p5);
        state.enemies = state.enemies.concat(foundEnemies);

        p5.noStroke();
        p5.fill(255);
        p5.triangle(
            state.shipScreen[0].x, state.shipScreen[0].y,
            state.shipScreen[1].x, state.shipScreen[1].y,
            state.shipScreen[2].x, state.shipScreen[2].y,
        );
        p5.circle(state.shipScreen[1].x, state.shipScreen[1].y, 5);
        const shipScreenCenter = getCoordinatesCenter(state.shipScreen);
        state.minions.forEach((minion, index) => {
            const directionVector = new Vector({ x: state.direction.x, y: state.direction.y }, p5);
            const mapVector = directionVector.copy().invert();
            minion.move(mapVector);
            minion.display();
            const distance = getDistance([shipScreenCenter, minion.coordinates])
            if (distance > 200 * state.screenFactor) {
                minion.setAction(() => minion.follow(shipScreenCenter, directionVector, index))
            } else if (distance < 100 * state.screenFactor) {
                minion.setAction(() => minion.doNothing());
            }
        });
        state.enemies.forEach((enemy, index) => {
            const directionVector = new Vector({ x: state.direction.x, y: state.direction.y }, p5);
            const mapVector = directionVector.copy().invert();
            enemy.move(mapVector);
            enemy.display();
            const distance = getDistance([shipScreenCenter, enemy.coordinates])
            if (distance < 400 * state.screenFactor) {
                enemy.setAction(() => enemy.rushToPoint(shipScreenCenter, directionVector, index))
            } else if (distance > 1000 * state.screenFactor) {
                enemy.setAction(() => enemy.doNothing());
            }
        });

        collision(state.shipScreen, scraps);

        if (state.stats.scraps <= 0) {
            state.isPaused = true;
            p5.noLoop();
            const ui: HTMLElement = <HTMLElement>document.querySelector('#ui');
            ui.style.display = 'none';
            const gameovertitle: HTMLElement = <HTMLElement>document.querySelector('#game-over-title');
            gameovertitle.style.display = 'flex';
            gameovertitle.addEventListener('click', () => {
                initializeObjects(p5);
            })
        }
        // debug
        // p5.text(`${p5.mouseX} - ${p5.mouseY}`, 20, p5.windowHeight - 40);
    };

    p5.mousePressed = () => {
        // suppress clicks on ui
        const uiRect = (<HTMLElement>document.querySelector('.left-section')).getBoundingClientRect();
        if (p5.mouseX > uiRect.left && p5.mouseX < uiRect.right
            && p5.mouseY > uiRect.top && p5.mouseY < uiRect.bottom) {
            return;
        }

        // get impulsion vector
        const shipSCenter = getCoordinatesCenter(state.shipScreen);
        const impulse = c((p5.mouseX - shipSCenter.x) / 100, (p5.mouseY - shipSCenter.y) / 100);

        // apply impulsion to ship vector
        const maxSpeed = 10;
        const tempDirection = c(state.direction.x + impulse.x, state.direction.y + impulse.y);
        const speed = Math.sqrt(tempDirection.x * tempDirection.x + tempDirection.y * tempDirection.y);
        if (speed <= maxSpeed) {
            state.direction.x = tempDirection.x;
            state.direction.y = tempDirection.y;
        } else {
            const newDirection = c(1 / ((speed / maxSpeed) / tempDirection.x), 1 / ((speed / maxSpeed) / tempDirection.y));
            state.direction.x = newDirection.x;
            state.direction.y = newDirection.y;
        }

        // update ship orientation
        rotateToTarget(state.ship, c(p5.mouseX, p5.mouseY), p5);
        rotateToTarget(state.shipScreen, c(p5.mouseX, p5.mouseY), p5);
    };

    p5.windowResized = () => {
        p5.resizeCanvas(p5.windowWidth, p5.windowHeight);
    }
}

new P5(sketch);
