import P5 from "p5";

function copyObject(objectToCopy: any) {
    return JSON.parse(JSON.stringify(objectToCopy));
}

export type Coordinates = {
    x: number,
    y: number,
};

export function c(x: number, y: number): Coordinates {
    return {
        x: x,
        y: y,
    };
}

export function cc(coords: Coordinates): Coordinates {
    return {
        x: coords.x,
        y: coords.y,
    };
}

export function getCoordinatesCenter(coordinates: Coordinates[]) {
    const count = coordinates.length;
    if (count === 0) {
        return c(0, 0);
    }
    const accumulatedCoordinates = coordinates
        .reduce((accumulatedCoordinates, currentCoordinates) =>
            c(accumulatedCoordinates.x + currentCoordinates.x, accumulatedCoordinates.y + currentCoordinates.y),
            c(0, 0),
        );
    return c(
        accumulatedCoordinates.x / count,
        accumulatedCoordinates.y / count
    );
}

export function lscale(coords: Coordinates[], scale: number): Coordinates[] {
    coords.forEach((coord: Coordinates) => {
        coord.x = coord.x * scale;
        coord.y = coord.y * scale;
    });
    return coords;
}

export function lscalec(coords: Coordinates[], scale: number): Coordinates[] {
    const center = getCoordinatesCenter(coords);
    coords.forEach((coord: Coordinates) => {
        coord.x = (coord.x - center.x) * scale;
        coord.y = (coord.y - center.y) * scale;
    });
    return coords;
}

export function coordsRote(coords: Coordinates[], center: Coordinates, angle: number, p5: P5): void {
    coords.forEach((coord: Coordinates) => {
        const oldX = coord.x;
        const oldY = coord.y;
        const rad = p5.radians(angle);
        coord.x = p5.cos(rad) * (oldX - center.x) - p5.sin(rad) * (oldY - center.y) + center.x;
        coord.y = p5.sin(rad) * (oldX - center.x) + p5.cos(rad) * (oldY - center.y) + center.y;
    });
}

export function rotateCoordinates(coords: Coordinates[], center: Coordinates, angle: number, p5: P5): Coordinates[] {
    return coords.map((coord: Coordinates) => {
        const oldX = coord.x;
        const oldY = coord.y;
        const rad = p5.radians(angle);
        return c(
            p5.cos(rad) * (oldX - center.x) - p5.sin(rad) * (oldY - center.y) + center.x,
            p5.sin(rad) * (oldX - center.x) + p5.cos(rad) * (oldY - center.y) + center.y
        );
    });
}

export function moveCoordinatesWithVector(coords: Coordinates[], movement: Vector): Coordinates[] {
    return coords.map((coord: Coordinates) => {
        return c(
            coord.x + movement.x,
            coord.y + movement.y,
        );
    });
}

export function moveCoordinatesAtPosition(coords: Coordinates[], movement: Coordinates): Coordinates[] {
    return coords.map((coord: Coordinates) => {
        return c(
            coord.x + movement.x,
            coord.y + movement.y,
        );
    });
}

export function coordinatesAngleDegrees(coord: Coordinates, center: Coordinates, p5: P5) {
    const radiansResult = p5.atan2(coord.x - center.x, coord.y - center.y);
    return (radiansResult * (180 / Math.PI) * -1) + 90;
}

export function addVector(coords: Coordinates[], vector: Coordinates) {
    coords.forEach((coord) => {
        coord.x = coord.x + vector.x;
        coord.y = coord.y + vector.y;
    });
}

export function sumVectors(vectorA: Coordinates, vectorB: Coordinates) {
    return c(
        vectorA.x + vectorB.x,
        vectorA.y + vectorB.y,
    );
}

export function buildVector(points: Coordinates[]): Coordinates {
    const center = points[0];
    const target = copyObject(points[1]);
    target.x -= center.x;
    target.y -= center.y;
    return target;
}

export function getDistance(points: Coordinates[]) {
    const vector = buildVector(points);
    return Math.sqrt(vector.x * vector.x + vector.y * vector.y);
}

export class Vector {
    vector: Coordinates;
    p5: P5;

    constructor(params: { x: number, y: number } | { coordinates: Coordinates[] }, p5: P5) {
        if (Object(params).hasOwnProperty('coordinates')) {
            const currentParams = (<{ coordinates: Coordinates[] }>params);
            this.vector = buildVector(currentParams.coordinates);
        } else {
            const currentParams = (<{ x: number, y: number }>params);
            this.vector = c(currentParams.x, currentParams.y);
        }
        this.p5 = p5;
    }

    copy(): Vector {
        return new Vector({x: this.vector.x, y: this.vector.y}, this.p5);
    }

    multiply(factor: number): void {
        this.vector.x *= factor;
        this.vector.y *= factor;
    }

    scale(factor: number): Vector {
        this.multiply(factor);
        return this;
    }

    addVector(vector: Vector): void {
        this.vector.x += vector.x;
        this.vector.y += vector.y;
    }

    sumVectors(vector: Vector): Vector {
        const newVector = this.copy();
        this.copy().addVector(vector);
        return newVector;
    }

    rotate(angle: number): void {
        coordsRote([this.vector], c(0, 0), angle, this.p5);
    }

    invert(): Vector {
        this.vector.x *= -1;
        this.vector.y *= -1;
        return this;
    }

    get size(): number {
        return Math.sqrt(this.vector.x * this.vector.x + this.vector.y * this.vector.y);
    }

    get x(): number {
        return this.vector.x;
    }

    get y(): number {
        return this.vector.y;
    }
}
