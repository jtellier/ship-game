import P5 from "p5";
import { rotateToTarget } from ".";
import { buildVector, c, cc, Coordinates, coordinatesAngleDegrees, getCoordinatesCenter, getDistance, lscalec, moveCoordinatesAtPosition, moveCoordinatesWithVector, rotateCoordinates, Vector } from "./geometry";

type ShipParameters = {
    maxAngleSpeed: number,
    maxSpeed: number,
}

type DrawFunction = (coords: Coordinates[]) => void;

export class Ship {
    coordinates: Coordinates;
    angle: number;
    movementVector: Vector;
    p5: P5;
    shapeCoordinates: Coordinates[];
    shapeCenter: Coordinates;
    draw: DrawFunction;
    currentAction: Function;
    id: string;
    speedFactor: number;

    constructor(coordinates: Coordinates, angle: number, movementVector: Vector, p5: P5, shapeCoordinates: Coordinates[], draw: DrawFunction, id: string) {
        this.coordinates = cc(coordinates);
        this.angle = angle;
        this.movementVector = movementVector.copy();
        this.p5 = p5;
        this.shapeCoordinates = shapeCoordinates.map(coord => cc(coord));
        this.shapeCenter = getCoordinatesCenter(this.shapeCoordinates);
        this.draw = draw;
        this.currentAction = this.doNothing;
        this.id = id;
        const minSide = (p5.windowHeight, p5.windowWidth)/2;
        this.speedFactor = (minSide-100) / (0.4*minSide);
    }

    doNothing() {
        this.movementVector.multiply(0.99);
    }

    setCoordinates(point: Coordinates): void {
        this.coordinates = cc(point);
    }

    setMovementVector(movementVector: Vector): void {
        this.movementVector = movementVector.copy();
    }

    setAngle(angle: number): void {
        this.angle = angle;
    }

    follow(pointToFollow: Coordinates, pointMovementVector: Vector, nudge: number): void {
        // vector from minion to mothership
        const targetVector = buildVector([this.coordinates, pointToFollow]);

        // speed vector from minion to mothership at least at mothership speed
        const maxSpeed = getDistance([this.coordinates, pointToFollow]);

        const targetVectorMove = new Vector(
            {
                y: ((11 / maxSpeed) * targetVector.y),
                x: ((11 / maxSpeed) * targetVector.x),
            },
            this.p5,
        );

        const newTargetVectorMove = targetVectorMove;

        // some variability to avoid minions superposition
        const variabilityVector = new Vector({
            x: this.p5.sin(newTargetVectorMove.x + nudge),
            y: this.p5.sin(newTargetVectorMove.y + nudge)
        }, this.p5);
        newTargetVectorMove.addVector(variabilityVector);

        // TODO limit angle vector with shipParameters
        if (this.movementVector.size === 0) {
            newTargetVectorMove.scale(0.5/newTargetVectorMove.size);
            this.movementVector = newTargetVectorMove;
        } else {
            const controlledSpeed = this.movementVector.copy().scale(1.1);
            if (controlledSpeed.size > newTargetVectorMove.size) {
                this.movementVector = newTargetVectorMove;
            } else {
                this.movementVector = controlledSpeed;
            }
        }
        this.angle = coordinatesAngleDegrees(this.movementVector, c(0,0), this.p5) + 90;
    }

    rushToPoint(pointToFollow: Coordinates, pointMovementVector: Vector, nudge: number): void {
        // vector from minion to mothership
        const targetVector = buildVector([this.coordinates, pointToFollow]);

        // speed vector from minion to mothership at least at mothership speed
        const maxSpeed = getDistance([this.coordinates, pointToFollow]);

        const targetVectorMove = new Vector(
            {
                y: ((8 / maxSpeed) * targetVector.y),
                x: ((8 / maxSpeed) * targetVector.x),
            },
            this.p5,
        );
        
        const newTargetVectorMove = targetVectorMove;// priorityVectorMove.sumVectors(nonPriorityVectorMove);

        // some variability to avoid minions superposition
        const variabilityVector = new Vector({
            x: this.p5.sin(newTargetVectorMove.x + nudge),
            y: this.p5.sin(newTargetVectorMove.y + nudge)
        }, this.p5);
        newTargetVectorMove.addVector(variabilityVector);

        // TODO limit angle vector with shipParameters
        if (this.movementVector.size === 0) {
            newTargetVectorMove.scale(0.5/newTargetVectorMove.size);
            this.movementVector = newTargetVectorMove;
        } else {
            const controlledSpeed = this.movementVector.copy().scale(1.1);
            if (controlledSpeed.size > newTargetVectorMove.size) {
                this.movementVector = newTargetVectorMove;
            } else {
                this.movementVector = controlledSpeed;
            }
        }
        this.angle = coordinatesAngleDegrees(this.movementVector, c(0,0), this.p5) + 90;
    }

    move(mapVector: Vector) {
        this.coordinates = moveCoordinatesWithVector([this.coordinates], mapVector)[0];
        this.currentAction();
        this.coordinates = moveCoordinatesWithVector([this.coordinates], this.movementVector)[0];
    }

    display() {
        const currentCoordinates: Coordinates[] = this.shapeCoordinates.map((coordinates) => cc(coordinates));
        const rotatedCoordinates: Coordinates[] = rotateCoordinates(currentCoordinates, this.shapeCenter, this.angle, this.p5);
        const movedCoordinates: Coordinates[] = moveCoordinatesAtPosition(rotatedCoordinates, this.coordinates);
        this.draw(movedCoordinates);
        // this.p5.fill(255);
        // this.p5.stroke(255);
        // this.p5.text(Math.floor(this.movementVector.size), this.coordinates.x, this.coordinates.y);
    }

    setAction(action: Function) {
        this.currentAction = action;
    }
}